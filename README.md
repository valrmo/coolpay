### Coolpay

This app uses Coolplay API and performs the following actions:

- Authenticate to Coolpay API
- Add recipients
- Send money to a recipient
- Check whether a payment was successful

### How to run the app

From the application root folder, run:

```sh
$ bundle
```

Run the following commands from the command line:

```sh
$ irb

$ require './app/coolpay_client'

$ client = CoolpayClient.new
```

Instantiating a new client will perform an authentication call and store the
auth token as an instance variable so it can be reused for following calls.

To add recipients:

```sh
$ client.add_recipient(name)
```

To send money to a recipient:

```sh
$ client.send_money(amount, currency, name)

```
This method returns a transaction id which can be stored and used to check
if the payment was successful or not.

To check payment status:

```sh
$ client.check_payment_status(id)

```

### The way I approached the problem and why I took certain decisions


I used TDD and decided to perform the authentication at instantiation time,
storing the auth token in an instance variable. The credentials to generate a new token are hardcoded within the class which is not ideal as they end up being in
source control.

I have decided to use net/http Gem to communicate with the API, because it's part of the Ruby standard library. I thought it was a good idea as I didn’t want to introduce new external dependencies but it turned out to be tricky to work with it and it took quite some time to get around headers, post and get requests with query parameters.
Next time I would definitely pick a gem with a nicer interface like `rest-client`.

I've discovered the payment endpoint works in an unexpected way. Every payment made has a `pending` status until a new payment is made. This makes it not possible to check immediately if a payment is successful or not. This led me to return a payment id so it can be stored and used later to check the payment status.

I've used webmock to stub api requests in testing environment as it's one of the most used mocking library and it integrates well with Rspec.

There is space for improvement as the code has some duplication, but rather than spending time trying to clean the code I would have rather migrated to a different API library and that, I think, goes beyond the scope of the test.

I enjoyed the test as it gave me the opportunity to learn to always consider more than one option before choosing a technology or a library.
