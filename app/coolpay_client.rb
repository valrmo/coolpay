require 'net/http'
require 'json'
require 'pry-byebug'

class CoolpayClient

  USERNAME = 'ValentinaR'
  API_KEY = '167431B5CA55B04F'

  def initialize
    @auth_token = JSON.parse(authenticate)
  end

  def add_recipient(name)
    uri = URI.parse("https://coolpay.herokuapp.com/api/recipients")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    request = Net::HTTP::Post.new(uri.path)
    request['Authorization'] = "Bearer #{auth_token['token']}"
    request['Content-Type'] = "application/json"
    request.body = {
      "recipient" => {
        "name" => name
      }
    }.to_json
    response = https.request(request)
    response.body
  end

  def send_money(amount, currency, name)
    uri = URI.parse("https://coolpay.herokuapp.com/api/payments")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true

    request = Net::HTTP::Post.new(uri.path)
    request['Authorization'] = "Bearer #{auth_token['token']}"
    request['Content-Type'] = "application/json"
    request.body = {
      "payment" => {
        "amount" => amount.to_f,
        "currency" => currency,
        "recipient_id" => "#{recipient_id(name)}"
      }
    }.to_json
    response = https.request(request)
    JSON.parse(response.body)["payment"]["id"]
  end

  def check_payment_status(id)
    if payment_status(id) == "paid"
       "Your payment has been successfuly paid"
    else
      "Your payment is failed"
    end
  end

  private

  attr_reader :auth_token

  def authenticate
    uri = URI("https://coolpay.herokuapp.com/api/login")
    request = Net::HTTP::Post.new(uri)

    request.set_form_data(
      {
        "username" => USERNAME,
        "apikey" => API_KEY
      }
    )

    response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
      http.request(request)
    end
    response.body
  end

  def payment_status(id)
    uri = URI.parse("https://coolpay.herokuapp.com/api/payments")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true

    request = Net::HTTP::Get.new(uri)
    request['Authorization'] = "Bearer #{auth_token['token']}"
    request['Content-Type'] = "application/json"

    response = https.request(request)
    payments = JSON.parse(response.body)["payments"]
    payments.find{|payment| payment['id'] == id}["status"]
  end

  def recipient_id(name)
    uri = URI('https://coolpay.herokuapp.com/api/recipients?name=name')
    params = { :name => name}
    uri.query = URI.encode_www_form(params)

    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true

    request = Net::HTTP::Get.new(uri)
    request['Authorization'] = "Bearer #{auth_token['token']}"
    request['Content-Type'] = "application/json"

    response = https.request(request)
    res = JSON.parse(response.body)
    res["recipients"].first["id"]
  end
end
