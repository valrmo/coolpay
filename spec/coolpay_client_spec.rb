require 'spec_helper'
require './app/coolpay_client'

describe CoolpayClient do
  subject(:coolpay_client) { CoolpayClient.new }

  let(:name) { 'Valentina' }

  let(:authentication_response) do
    { "token": "484265f2-e633-429e-bfc0-6348804cae99" }.to_json
  end

  let(:recipient_response) do
    {
      "recipient": {
        "name": name,
        "id": "03a65989-22bd-4343-8318-619e99fff06f"
      }
    }.to_json
  end

  let(:payment_response) do
    {
      "payment": {
        "status": "processing",
        "recipient_id": "03a65989-22bd-4343-8318-619e99fff06f",
        "id": "0207af5e-e593-4f28-bc95-7dfed5fdf5d5",
        "currency": "GBP",
        "amount": "10.5"
      }
    }.to_json
  end

  let(:id_response) do
    {
      "recipients": [
        {
          "name": name,
          "id": "0207af5e-e593-4f28-bc95-7dfed5fdf5d5"
        }
      ]
    }.to_json
  end

  let(:payment_list_response) do
    {
      "payments": [
        {
          "status": "paid",
          "recipient_id": "0207af5e-e593-4f28-bc95-7dfed5fdf5d5",
          "id": "0207af5e-e593-4f28-bc95-7dfed5fdf5d5",
          "currency": "GBP",
          "amount": "10.5"
        },
        {
          "status": "paid",
          "recipient_id": "eccb67ef-9618-4a17-a4ed-924aa76627af",
          "id": "dcd5696a-c2ac-4566-9114-e45028a3e2a6",
          "currency": "GBP",
          "amount": "10"
        }
      ]
    }.to_json
  end

  before do
    stub_request(:post, 'https://coolpay.herokuapp.com/api/login')
      .to_return(status: 200, body: authentication_response)

    stub_request(:post, 'https://coolpay.herokuapp.com/api/recipients')
      .to_return(status: 200, body: recipient_response)

    stub_request(:get, 'https://coolpay.herokuapp.com/api/recipients?name=Valentina')
      .to_return(status: 200, body: id_response)

    stub_request(:post, 'https://coolpay.herokuapp.com/api/payments')
      .to_return(status: 200, body: payment_response)

    stub_request(:get, 'https://coolpay.herokuapp.com/api/payments')
      .to_return(status: 200, body: payment_list_response)
  end

  it 'creates a recipient' do
    expect(coolpay_client.add_recipient(name)).to eq(recipient_response)
  end


  it 'sends money to recipient' do
    expect(coolpay_client.send_money(10.5, "GBP", "Valentina")).to eq("0207af5e-e593-4f28-bc95-7dfed5fdf5d5")
  end

  it 'checks payment status' do
    expect(coolpay_client.check_payment_status("dcd5696a-c2ac-4566-9114-e45028a3e2a6")).to eq("Your payment has been successfuly paid")
  end

end
